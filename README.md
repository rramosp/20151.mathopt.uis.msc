## Uso infraestructura **GUANE** @ **UIS**

si, por ejemplo, tu número de estudiante es el 3 usa la cuenta `estudiante3` y el puerto `9903` en los comandos siguientes. Verifica en [http://www.sc3.uis.edu.co/servicios/hardware/estado/](http://www.sc3.uis.edu.co/servicios/hardware/estado/) las máquinas que están disponibles. Supongamos que elegimos `guane02`.

Usa la cuenta compartida para entrar en la máquina `toctoc` y en la máquina `guane` que son los puntos de entrada para la infraestructura de la UIS:

    :::console
    ssh estudiante3@toctoc.grid.uis.edu.co -o ServerAliveInterval=30 
    ssh guane 
    oarsub -I -p "network_address='guane02'"
    git clone https://bitbucket.org/rramosp/20151.mathopt.uis.msc.git
    export THEANO_FLAGS=mode=FAST_RUN,device=gpu,floatX=float32
    ipython notebook --ip '*' --port 9903 --no-browser

Anota la máquina que te asignó (por ejemplo `guane02') y abre otra sesión desde tu terminal para establecer los puentes de los puertos

    :::console
    ssh estudiante3@toctoc.grid.uis.edu.co -o ServerAliveInterval=30 -L9903:localhost:9903
    ssh guane -L9903:guane14:9903

Abre la siguiente dirección en un browser de tu computadora de escritorio

    :::console
    http://localhost:9903